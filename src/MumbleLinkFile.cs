﻿using UnityEngine;
using System;
using System.Text;
using System.IO;
using System.IO.MemoryMappedFiles;
using System.Runtime.InteropServices;

namespace mumblelib
{
	public interface LinkFile : IDisposable
	{
		uint UIVersion { set; }
		void Tick();
		Vector3 CharacterPosition { set; }
		Vector3 CharacterForward { set; }
		Vector3 CharacterTop { set; }
		string Name { set; }
		Vector3 CameraPosition { set; }
		Vector3 CameraForward { set; }
		Vector3 CameraTop { set; }
		string ID { set; }
		string Context { set; }
		string Description { set; }
	}

	public interface LinkFileManager
	{
		LinkFile Open();
	}

	public unsafe class UnixLinkFileManager : LinkFileManager
	{

		public LinkFile Open()
		{
			return new UnixLinkFile();
		}
	}

	public class WindowsLinkFileManager : LinkFileManager
	{
		public LinkFile Open()
		{
			return new WindowsLinkFile();
		}
	}

	unsafe class UnixLinkFile : IDisposable, LinkFile
	{
		[DllImport("librt")]
		private static extern int shm_open([MarshalAs(UnmanagedType.LPStr)] string name, int oflag, uint mode);
		[DllImport("libc")]
		private static extern uint getuid();
		[DllImport("libc")]
		private static extern void* mmap(void* addr, long length, int prot, int flags, int fd, long off);
		[DllImport("libc")]
		private static extern void* munmap(void* addr, long length);
		[DllImport("libc")]
		private static extern int close(int fd);

		private bool disposed;
		private readonly LinuxLinkMemory* ptr;
		private readonly int fd;

		unsafe public UnixLinkFile()
		{
			fd = shm_open("/MumbleLink." + getuid(), 2, 384);
			if (fd < 0)
			{
				throw new Exception("failed to open shm");
			}
			ptr = (LinuxLinkMemory*)mmap(null, Marshal.SizeOf<LinuxLinkMemory>(), 3, 1, fd, 0);
		}

		public uint UIVersion { set { ptr->uiVersion = value; } }
		public void Tick() { ptr->uiTick++; }
		public Vector3 CharacterPosition { set { Util.SetVector3(ptr->fAvatarPosition, value); } }
		public Vector3 CharacterForward { set { Util.SetVector3(ptr->fAvatarFront, value); } }
		public Vector3 CharacterTop { set { Util.SetVector3(ptr->fAvatarTop, value); } }
		public string Name { set { Util.SetString(ptr->name, value, 256, Encoding.UTF32); } }
		public Vector3 CameraPosition { set { Util.SetVector3(ptr->fCameraPosition, value); } }
		public Vector3 CameraForward { set { Util.SetVector3(ptr->fCameraFront, value); } }
		public Vector3 CameraTop { set { Util.SetVector3(ptr->fCameraTop, value); } }
		public string ID { set { Util.SetString(ptr->id, value, 256, Encoding.UTF32); } }
		public string Context { set { Util.SetContext(ptr->context, &ptr->context_len, value); } }
		public string Description { set { Util.SetString(ptr->description, value, 2048, Encoding.UTF32); } }

		public void Dispose()
		{
			if (!disposed)
			{
				munmap((void*)ptr, Marshal.SizeOf<LinuxLinkMemory>());
				close(fd);
				disposed = true;
			}
		}

		~UnixLinkFile()
		{
			Dispose();
		}

	}

	unsafe class WindowsLinkFile : IDisposable, LinkFile
	{
		private readonly MemoryMappedFile memoryMappedFile;
		private readonly WindowsLinkMemory* ptr;

		public unsafe WindowsLinkFile()
		{
			memoryMappedFile = MemoryMappedFile.CreateOrOpen("MumbleLink", Marshal.SizeOf<WindowsLinkMemory>());
			byte* tmp = null;
			memoryMappedFile.CreateViewAccessor().SafeMemoryMappedViewHandle.AcquirePointer(ref tmp);
			ptr = (WindowsLinkMemory*)tmp;
		}

		public uint UIVersion { set { ptr->uiVersion = value; } }
		public void Tick() { ptr->uiTick++; }
		public Vector3 CharacterPosition { set { Util.SetVector3(ptr->fAvatarPosition, value); } }
		public Vector3 CharacterForward { set { Util.SetVector3(ptr->fAvatarFront, value); } }
		public Vector3 CharacterTop { set { Util.SetVector3(ptr->fAvatarTop, value); } }
		public string Name { set { Util.SetString(ptr->name, value, 256, Encoding.Unicode); } }
		public Vector3 CameraPosition { set { Util.SetVector3(ptr->fCameraPosition, value); } }
		public Vector3 CameraForward { set { Util.SetVector3(ptr->fCameraFront, value); } }
		public Vector3 CameraTop { set { Util.SetVector3(ptr->fCameraTop, value); } }
		public string ID { set { Util.SetString(ptr->id, value, 256, Encoding.Unicode); } }
		public string Context { set { Util.SetContext(ptr->context, &ptr->context_len, value); } }
		public string Description { set { Util.SetString(ptr->description, value, 2048, Encoding.Unicode); } }

		private bool disposed;
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		~WindowsLinkFile()
		{
			Dispose(false);
		}

		protected virtual void Dispose(bool disposing)
		{
			if (!this.disposed)
			{
				if (disposing)
				{
					memoryMappedFile.Dispose();
				}
				disposed = true;
			}
		}
	}

	public unsafe struct LinuxLinkMemory
	{
		public uint uiVersion;

		public uint uiTick;

		public fixed float fAvatarPosition[3];

		public fixed float fAvatarFront[3];

		public fixed float fAvatarTop[3];

		public fixed uint name[256];

		public fixed float fCameraPosition[3];

		public fixed float fCameraFront[3];

		public fixed float fCameraTop[3];

		public fixed uint id[256];

		public uint context_len;

		public fixed byte context[256];

		public fixed uint description[2048];
	}

	public unsafe struct WindowsLinkMemory
	{
		public uint uiVersion;

		public uint uiTick;

		public fixed float fAvatarPosition[3];

		public fixed float fAvatarFront[3];

		public fixed float fAvatarTop[3];

		public fixed ushort name[256];

		public fixed float fCameraPosition[3];

		public fixed float fCameraFront[3];

		public fixed float fCameraTop[3];

		public fixed ushort id[256];

		public uint context_len;

		public fixed byte context[256];

		public fixed ushort description[2048];
	}

	class Util
	{
		public static unsafe void SetVector3(float* output, Vector3 input)
		{
			output[0] = input.x;
			output[1] = input.y;
			output[2] = input.z;
		}

		public static unsafe void SetString<T>(T* output, string input, int max, Encoding encoding)
		where T : unmanaged
		{
			byte[] bytes = encoding.GetBytes(input + "\u0000");
			Marshal.Copy(bytes, 0, new IntPtr(output), Math.Min(bytes.Length, max * Marshal.SizeOf<T>()));
		}

		public static unsafe void SetContext(byte* output, uint* len, string input)
		{
			byte[] bytes = Encoding.UTF8.GetBytes(input);
			*len = (uint)Math.Min(bytes.Length, 256);
			Marshal.Copy(bytes, 0, new IntPtr(output), (int)*len);
		}
	}
}
